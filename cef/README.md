This is a good place to put your CEF builds and refer to them here when using tools\build_win.bat.

This folder (except this file) is ignored by Mercurial (via .hgignore) so it makes commands like `hg status` less confusing when checking which files you have added or modified.
